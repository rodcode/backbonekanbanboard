var gulp = require('gulp');
var eslint = require('gulp-eslint');
var browserify = require('gulp-browserify');
var stringify = require('stringify');
 
gulp.task('lint', () => {
    return gulp.src(['js/**/*.js','!node_modules/**'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('build', function() {

    gulp.src('./js/app/app.js')
    .pipe(browserify({       
        insertGlobals : true,
        debug : true,
        transform: ['stringify'],
        shim:{
            jquery:{
                path: 'node_modules/jquery/dist/jquery.min.js',
                exports: '$'
            }
        }
    }))
    .pipe(gulp.dest('./build/js'));
});

gulp.task('karma', function(done){
  var config = {
    testDir: 'test'
    };
    var karma = require('karma');  
    var karmaOpts = {
      configFile:  process.cwd() + "/" +   config.testDir + '/karma.conf.js',
      singleRun: false,
    };
    new karma.Server(karmaOpts, done).start();
});

gulp.task('watch', function() {
    gulp.watch(['js/**/*.js'], ['lint']);
});