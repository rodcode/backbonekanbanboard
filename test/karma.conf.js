module.exports = function(config) {
  config.set({
    basePath: '../',
    frameworks: ['jasmine', 'browserify'],
    files: [
      {pattern: 'js/specs/*spec.js'}     
    ],
    preprocessors: {
      'js/specs/*spec.js': [ 'browserify' ]
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: [
      'Chrome',
    ],
    singleRun: false,
    autoWatch: true,  
    concurrency: Infinity,
    browserify: {
      debug: true,
      transform: [ 'stringify' ]
    }
  });
}
