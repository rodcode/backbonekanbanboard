'use strict';

var Backbone = require('backbone');
var BackboneLocalStorage = require('backbone.localstorage');

var myModule = {};
function _getConstructorLocalStorage(modelType){
    return function (){
        Backbone[modelType].apply(this, arguments);
        if(this.localStorageNamespace){
            this.localStorage = new BackboneLocalStorage(this.localStorageNamespace);
        }
    };
}

myModule.Model = Backbone.Model.extend({
    constructor: _getConstructorLocalStorage('Model')
}); 

myModule.Collection = Backbone.Collection.extend({
    constructor: _getConstructorLocalStorage('Collection')
});

module.exports = myModule;