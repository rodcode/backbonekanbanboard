'use strict';

var Backbone = require('backbone');
Backbone.$ = require('jquery');
var Bootstrap = require('bootstrap');

module.exports = Backbone.View.extend({
    tagName: 'div',
    className: 'modal fade',
    attributes:{ tabindex: -1, role: 'dialog'},
    id: 'story-form',
    events:{
        'click .save' : 'accept'
    },
    initialize: function(options){
        this.options = options;
        this.$el.on('hidden.bs.modal',this.remove.bind(this));
        this.render();
    },
    render: function(){
        this.$el.html(this.options.template);
    },
    show : function(){
        this.$el.modal('show');
    },
    accept: function(){
        this.trigger('accept-modal');
        this.$el.modal('hide');
    }
});