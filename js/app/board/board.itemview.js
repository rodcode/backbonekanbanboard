var _ = require('underscore');
var Backbone = require('backbone');

var StageCollection = require('../stage/stage.collection');
var StageView = require('../stage/stage.view');
var ItemTemplate = require('./templates/boardItem.html');

module.exports = Backbone.View.extend({
    tagName: 'li',
    template: _.template(ItemTemplate),
    attributes: function () {
        return {
            class: this.model.get('active') ? 'active' : 'no-active'
        };
    },
    initialize: function () {
        this.listenTo(this.model,'destroy', this.delete);
        this.listenTo(this.model,'change:active', this.updateModel);
        this.listenTo(this.model,'change:title', this.render);
        if (this.model.get('active')) {
            this.initStages();
        }
    },
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    initStages: function () {
        this.stageView = new StageView({
            collection: new StageCollection()
        },
            {
                boardId: this.model.get('id')
            });
        $('#app').html(this.stageView.el);
    },
    updateModel: function (model) {
        this.removeStageView();
        this.$el.attr(_.result(this, 'attributes'));
        if (model.get('active')) {               
            this.initStages();
        }
    },
    removeStageView: function () {
        if (this.stageView) {
            this.stageView.delete();
        }
    },
    delete: function () {
        this.stageView.delete();
        this.remove();
    }
});