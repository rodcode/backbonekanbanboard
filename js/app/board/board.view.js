'use strict';

var _ = require('underscore');
var Backbone = require('backbone');
 
var StageCollection = require('../stage/stage.collection');
var BoardCollectionView = require('./board.collectionview');
var BoardView = require('./templates/board.html');

module.exports = Backbone.View.extend({
    template: _.template(BoardView),
    initialize: function () {
        this.listenTo(this.collection,'reset', this.render);
        this.listenTo(this.collection,'add', this.addOne);
    },
    events: {
        'click .destroy': 'destroy',
        'click .change': 'change',
        'click .new': 'new_board'
    },
    change: function () {
        var model = this.getActivePanel();
        var title = window.prompt('Nombre para el tablero ', model.get('title'));
        if (title) {
            model.save({ 'title': title });
        }
    },
    destroy: function () {
        if (window.confirm('¿Desea eliminar el tablero?')) {
            var model = this.getActivePanel();
            model.destroy();
            window.location.href = '/';
        }
    },
    new_board: function () {
        var board_title = window.prompt('Nombre para el tablero');
        if (board_title) {
            var boardModel = this.collection.create(
                { 'title': board_title },
                { wait: true }
            );
            this.createStages(boardModel.get('id'));
        }
    },
    render: function () {
        this.$el.html(this.template());
        this.boardCollectionView = new BoardCollectionView({
            collection: this.collection
        });
        this.$('#board-items').html(this.boardCollectionView.render().el);
        return this;
    },
    addOne: function (model) {
        this.boardCollectionView.renderOne(model);
    },
    getActivePanel: function () {
        return this.collection.findWhere({ active: true });
    },
    makeBoardActive: function (id) {
        this.collection.invoke('set', { active: false });
        var activeBoard = this.collection.findWhere({ id: id });
        activeBoard.set('active', true);
    },
    createStages: function (id) {
        this.stages = new StageCollection();
        this.stages.create(
            { boardId: id, title: 'TODO' }
        );
        this.stages.create(
            { boardId: id, title: 'IN PROGRESS' }
        );
        this.stages.create(
            { boardId: id, title: 'DONE' }
        );
    }
});