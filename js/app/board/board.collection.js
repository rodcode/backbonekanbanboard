'use strict';

var BackboneLocalStorage = require('../../shared/localStorageExtension');

var Board = require('./board.model');

module.exports = BackboneLocalStorage.Collection.extend({
    model: Board,
    url: 'api/board',
    localStorageNamespace: 'boardStorage'
});