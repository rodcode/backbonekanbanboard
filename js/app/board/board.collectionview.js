'use strict';

var _ = require('underscore');
var Backbone = require('backbone');
var BoardItemView = require('./board.itemview');

module.exports = Backbone.View.extend({
    tagName: 'ul',
    id: 'board-list',
    className: 'nav nav-tabs',
    render: function () {
        _.forEach(this.collection.models, function (item) {
            this.$el.append((new BoardItemView({ model: item })).render().el);
        }, this);
        return this;
    },
    renderOne: function (model) {
        this.$el.append(new BoardItemView({ model: model }).render().el);
    }
});