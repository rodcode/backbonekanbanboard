'use strict';

var Backbone = require('backbone');
var _ = require('underscore');

var StoryCollection = require('../story/story.collection');
var Story = require('../story/story.model');
var StoryCollectionView = require('../story/story.collectionview');
var StageTemplate = require('./templates/stageView.html');

module.exports = Backbone.View.extend({
    className: 'stage',
    template: _.template(StageTemplate),
    initialize: function () {
        this.collection = new StoryCollection();
        this.collection.fetch();
        var filtered = this.collection.where({ stageId: this.model.get('id') });
        this.collection.reset(filtered);
        this.render();
    },
    events: {
        'click .new': 'new_story'
    },
    new_story: function () {
        var story = new Story();
        story.save({ stageId: this.model.get('id'), order: this.collection.length - 1 });
        this.collection.add(story);
        this.renderOne(story);
    },
    render: function () {
        this.storyCollectionView = new StoryCollectionView(
            {
                collection: this.collection
            },
            {
                stageId: this.model.get('id')
            }
        );
        this.$el.html(this.template(this.model.toJSON()));
        this.$('.posits-story').html(this.storyCollectionView.el);
        return this;
    },
    renderOne: function (story) {
        this.storyCollectionView.addOne(story);
    },
    delete: function () {
        this.storyCollectionView.delete();
        this.remove();
    }
});