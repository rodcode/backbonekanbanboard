'use strict';

var BackboneLocalStorage = require('../../shared/localStorageExtension');

module.exports = BackboneLocalStorage.Model.extend({
    urlRoot: 'api/stage'
});