'use strict';

var Backbone = require('backbone');
var _ = require('underscore');

var StageItemView = require('./stage.itemview');

module.exports = Backbone.View.extend({
    id: 'stages',
    initialize: function (attributes, options) {
        this.boardId = options.boardId;
        this.listenTo(this.collection,'reset', this.render);
        this.childViews = [];
        this.collection.fetch();
        var filtered = this.collection.where({ 'boardId': this.boardId });
        this.collection.reset(filtered);
    },
    render: function () {
        _.each(this.collection.models, function (stage) {
            var childView = new StageItemView({ model: stage });
            this.childViews.push(childView);
            this.$el.append(childView.el);
        }, this);
        return this;
    },
    delete: function () {
        _.each(this.childViews, function (item) {
            item.delete();
        });
        this.remove();
    }
});