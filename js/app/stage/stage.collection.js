'use strict';

var BackboneLocalStorage = require('../../shared/localStorageExtension');

var Stage = require('./stage.model');

module.exports = BackboneLocalStorage.Collection.extend({
    url: 'api/stage',
    model: Stage,
    localStorageNamespace: 'stageStorage'
});