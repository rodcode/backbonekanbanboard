'use strict';

var Backbone = require('backbone');
var $ = require('jquery');
Backbone.$ = $;

var BoardCollection = require('./board/board.collection');
var BoardView = require('./board/board.view');

var AppRouter = Backbone.Router.extend({
    routes: {
        'board/:id': 'board'
    },
    initialize: function () {
        this.boards = new BoardCollection();
        this.boardsView = new BoardView({
            collection: this.boards
        });
        this.showBoards();
    },
    showBoards: function () {
        $('#header').html(this.boardsView.el);
        this.boards.fetch({reset: true});
    },
    board: function (id) {
        this.boardsView.makeBoardActive(id);
    }
});

module.exports = new AppRouter();
Backbone.history.start();