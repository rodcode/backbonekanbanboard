'use strict';

var BackboneLocalStorage = require('../../shared/localStorageExtension');

module.exports = BackboneLocalStorage.Model.extend({
    defaults: {
        description: '',
        color: 'yellow',
        order: 0,
        duration: 0
    },
    urlRoot: 'api/story',
    localStorageNamespace: 'storyStorage'
});