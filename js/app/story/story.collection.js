'use strict';

var BackboneLocalStorage = require('../../shared/localStorageExtension');

var Story = require('./story.model');

module.exports = BackboneLocalStorage.Collection.extend({
    model: Story,
    url: 'api/story',
    comparator: function (story) {
        return story.get('order');
    },
    localStorageNamespace: 'storyStorage'
});