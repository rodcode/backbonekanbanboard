'use strict';

var Backbone = require('backbone');
var _ = require('underscore');
require('jquery-ui');
require('jquery-ui/ui/data');
require('jquery-ui/ui/scroll-parent');
require('jquery-ui/ui/widgets/mouse');
require('jquery-ui/ui/widgets/sortable');

var StoryItemView = require('./story.itemview');
var Story = require('./story.model');

module.exports = Backbone.View.extend({
    tagName: 'ul',
    className: 'story-set',
    initialize: function (attributes, options) {
        this.childrenView = [];
        this.stageId = options.stageId;
        this.$el.sortable({
            items: 'li',
            connectWith: '.story-set',
            remove: this._remove.bind(this),
            receive: this.receive.bind(this),
            update: this.update.bind(this),
        });
        this.render();
    },
    _remove: function (event, ui) {
        var modelAttributes = $(ui.item).data('model');
        this.collection.remove(modelAttributes.id, { silent: true });
    },
    receive: function (event, ui) {
        var modelStory = $(ui.item).data('model');
        modelStory.set('stageId', this.stageId);
        modelStory.save();
        this.collection.add(modelStory, { silent: true });
    },
    update: function () {
        this.$('.story').each(function (index, item) {
            var modelStory = this.collection.findWhere({ id: $(item).data('model').id });
            if (modelStory) {
                modelStory.save({ order: index });
            }
        }.bind(this));
    },
    render: function () {
        _.each(this.collection.models, function (story) {
            var storyItem = new StoryItemView({ model: story });
            this.childrenView.push(storyItem);
            this.$el.append(storyItem.render().el);
        }, this);
        this.$el.append('<li class="drop"></li>');
        return this;
    },
    addOne: function (story) {
        var storyItem = new StoryItemView({ model: story });
        this.childrenView.push(storyItem);
        this.$el.append(storyItem.render().el);
    },
    delete: function () {
        _.each(this.childrenView, function (item) {
            item.remove();
        }, this);
        this.remove();
    }
});