'use strict';

var Backbone = require('backbone');
var _ = require('underscore');

var ModalView = require('../../shared/modalView');
var StoryTemplate = require('./templates/storyItem.html');
var StoryForm = require('./templates/storyForm.html');

module.exports = Backbone.View.extend({
    tagName: 'li',
    className: 'story',
    template: _.template(StoryTemplate),
    events: {
        'click': 'edit_story'
    },
    edit_story: function () {
        var template = _.template(StoryForm);
        var modal = new ModalView({ template: template(this.model.toJSON()) });
        modal.show();
        this.listenTo(modal, 'accept-modal', this.updateModels);
    },
    updateModels: function () {
        var data = {};
        var form_data = $('#story-form form').serializeArray();
        for (var i in form_data) {
            data[form_data[i].name] = form_data[i].value;
        }
        this.model.save(data);
    },
    initialize: function () {
        this.listenTo(this.model,'change', this.render);
    },
    render: function () {
        this.$el.data('model', this.model);
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});