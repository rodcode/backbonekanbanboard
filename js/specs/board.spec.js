var BoardCollection = require('../app/board/board.collection');
var _ = require('underscore');

describe('just checking', function () {

    it('works for collection', function () {
        var Board = new BoardCollection();
        Board.add({ title: 'ExampleBoard' });
        expect(Board.length > 0).toBe(true);
    });

    it('works for underscore', function () {
        // just checking that _ works
        expect(_.size([1, 2, 3])).toEqual(3);
    });

});
